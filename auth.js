const jwt = require('jsonwebtoken');
//user defined string data will be used to create our JSON web tokens
//used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = 'CourseBookingAPI';

//JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to the other part pf server

//TOKEN creation
//Analogy = Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {
    
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };



    return jwt.sign(data, secret, {})


}