const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//check if the email already exists
/**
 * Steps:
 * 1. use mongoose "find" method to find duplicate emails
 * 2. use the "then" method to send a response back to the client based on the result of the find method (email already exists/not existing)
 */

module.exports.checkEmailExists = (reqBody) => {
    return User.find( {email: reqBody.email } ).then((result) => {
        //The "find" method returns a record if a match is found
        if(result.length > 0){
            return true;
        }else{
            //No duplicate email found
            //the user is not yet registered in the database
            return false;
        }
    })
}

//user registration
/**
 * 
 */
//uses the information from the request body to provide all necessary information
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        //10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    //saves the created object to our database
    return newUser.save().then((user, error) => {
        //user registration failed
        if(error){
            return false;
        }else{
            //user registration is successful
            return true;
        }
    })
}


//User authentication
/**
 * steps:
 * 1. check the database if the user email exists
 * 2. compare the password provided in the login form with the password stored in the database
 * 3. Generate/return a JSON web token if the user is successfuly logged in and return false if not
 */

module.exports.loginUser = (reqBody) => {
    return User.findOne( {email: reqBody.email} ).then(result => {
        //if user does not exist
        if(result == null){
            return false;
        }else{
            //user exists
            //create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
            //"compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            //A good practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
            //example: isAdmin, isDone, areDone
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            //if the passwords match/result of the above code is true
            if(isPasswordCorrect){
                //generate an access token
                //use the "createAccessToken" method defined in the 'auth.js' file
                //returning an object back to the frontend
                //we will use the mongoose method "toObject" = it converts the mongoose object into a plain javascript object
                return { accessToken: auth.createAccessToken(result.toObject()) }
            }else{
                //password do not match
                return false;
            }
        }
    })
}

/**ACTIVITY */
// module.exports.getProfile = (usersId) => {
    
//     return User.findById(usersId).then(result => {
//         return result;
//     })
// }
module.exports.getProfile = (reqBody) => {
    
    return User.findOne({ _id: reqBody.id }).then(result => {

        if(result == null){
            return false;
        }else{
            result.password = "";

            return result;
            
        }
       
    })
}